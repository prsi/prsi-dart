import 'dart:async';

import 'package:console/console.dart';

import '../card/card.dart';
import '../card/card_color.dart';
import '../card/card_type.dart';
import '../player/player.dart';
import '../player/players.dart';
import '../translations/translation.dart';
import 'game_interface.dart';

class CLIInterface implements GameInterface<Player> {
  final ConsoleAdapter _console;
  final Translation _translation;

  CLIInterface._(this._translation, this._console);

  factory CLIInterface(Translation translation) {
    Console.init();
    return CLIInterface._(translation, Console.adapter);
  }

  void _clearConsole() {
    _console.writeln("\x1B[2J\x1B[0;0H");
  }

  @override
  void addCardToDeck(Player player, Card retrievedCard) {
    // TODO: implement addCardToDeck
  }

  @override
  void currentPlayer(Player player) {
    _clearConsole();

    _console.write(_translation.getTranslation('player'));
    _console.write(' ');
    _console.write(player.name);
    _console.write(' ');
    _console.write(_translation.getTranslation('plays'));
  }

  @override
  void gameOver(Players players, int round) {
    _clearConsole();

    _console.writeln(_translation.getTranslation('game_over'));
    _console.writeln('');
    _console.write(_translation.getTranslation('winner_is'));

    final winners = players.winner;

    if(winners.length > 1) {
      var i = 1;

      while(winners.isNotEmpty) {
        _console.writeln('${i++}. ${winners.removeFirst().name}');
      }
    } else {
      _console.writeln(winners.first.name);
    }
  }

  @override
  void gameStart() {
    _clearConsole();

    _console.writeln(_translation.getTranslation("game_start"));
    _console.writeln('');
    _console.write(_translation.getTranslation("prompt_for_continue"));
    _console.readByte();
  }

  @override
  Future<Card?> letPlayerSelectCard(Player player, Card card, CardColor? colorOnChanger, int? overChargeCount) async {
    final isSpecial = card.isSpecial && overChargeCount != null;

    _console.writeln('');

    if (card.isSuperSpecial && colorOnChanger != null) {
      _console.write(_translation.getTranslation("current_color"));
      _console.writeln(":");
      _console.writeln(_translation.getCardColorName(colorOnChanger));
    } else {
      _console.write(_translation.getTranslation('current_card'));
      _console.writeln(':');
      _console.writeln(_translation.getCardName(card));
    }
    _console.writeln('');

    final cardsForSelect = <Card>[];
    final remainingCards = <Card>[];

    if (card.isSuperSpecial) {
      for (Card card1 in player.deck) {
        if(card1.isSuperSpecial || card1.color == colorOnChanger) {
          cardsForSelect.add(card1);
        } else {
          remainingCards.add(card1);
        }
      }
    } else if (isSpecial) {
      for (Card card1 in player.deck) {
        if (card1.type == card.type) {
          cardsForSelect.add(card1);
        } else {
          remainingCards.add(card1);
        }
      }
    } else {
      for (Card card1 in player.deck) {
        if (card1.isSuperSpecial || card.compareTo(card1) == 0) {
          cardsForSelect.add(card1);
        } else {
          remainingCards.add(card1);
        }
      }
    }

    if (cardsForSelect.isNotEmpty) {
      _console.write(_translation.getTranslation('cards_for_select'));
      _console.writeln(':');

      for (int i = 0; i < cardsForSelect.length; i++) {
        _console.writeln('$i) ${_translation.getCardName(cardsForSelect[i])}');
      }

      _console.writeln('');

      if (remainingCards.isNotEmpty) {
        _console.write(_translation.getTranslation('remaining_cards'));
        _console.writeln(':');

        for (Card remainingCard in remainingCards) {
          _console.writeln('\t${_translation.getCardName(remainingCard)}');
        }
      }

      _console.writeln('');

      _console.writeln(_translation.getTranslation('select_card'));
      _console.write(': ');

      final response = _console.read();

      if (response != null) {
        final index = int.tryParse(response);

        if (index != null && index >= 0 && index < cardsForSelect.length) {
          final selectedCard = cardsForSelect[index];

          if(selectedCard.type == CardType.menic) {
            colorOnChanger = selectedCard.color;
          }

          return selectedCard;
        }
      }
    } else {
      _console.writeln(_translation.getTranslation(
          isSpecial ? 'no_cards_to_play_on_overcharge' : 'no_cards_to_play'));

      _console.writeln('');

      _console.write(_translation.getTranslation('remaining_cards'));
      _console.writeln(':');

      for (Card remainingCard in player.deck) {
        _console.writeln('\t${_translation.getCardName(remainingCard)}');
      }

      _console.writeln('');

      _console.writeln(_translation.getTranslation('prompt_for_continue'));
      _console.read();
    }

    return null;
  }

  @override
  Future<CardColor?> letPlayerSelectColor() async {
    _console.writeln('');

    _console.write(_translation.getTranslation('colors_for_select'));
    _console.writeln(':');

    final colors = CardColor.values;

    for (int i = 0; i < colors.length; i++) {
      _console.writeln('$i) ${_translation.getCardColorName(colors[i])}');
    }

    _console.writeln('');

    _console.writeln(_translation.getTranslation('select_color'));
    _console.write(': ');

    var response = _console.read();

    if (response != null) {
      final index = int.tryParse(response);

      if (index != null && index >= 0 && index < colors.length) {
        return colors[index];
      }
    }

    return null;
  }

  @override
  void placeCard(Card selectedCard) {
    // TODO: implement placeCard
  }

  @override
  void playerRetrievedOverChargedCards(Player player, int count) {
    // TODO: implement playerRetrievedOverChargedCards
  }

  @override
  void showFirstCard(Card currentCard) {
    // TODO: implement showFirstCard
  }

}
