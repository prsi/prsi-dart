import 'dart:collection';

import '../card/card.dart';

class Player {
  final List<Card> _deck;
  final String _name;
  int _deckSize;

  Player(this._name)
      : _deck = [],
        _deckSize = 0;

  void clearDeck() {
    _deck.clear();
  }

  void addCardToDeck(Card card) {
    _deck.add(card);
    _deckSize++;
  }

  void addCardsToDeck(List<Card> cards) {
    _deck.addAll(cards);
    _deckSize += cards.length;
  }

  void removeCardFromDeck(Card card) {
    _deck.remove(card);
    _deckSize--;
  }

  String get name => _name;
  bool get isPlaying => _deckSize > 0;
  List<Card> get deck => UnmodifiableListView<Card>(_deck);

  @override
  String toString() {
    return 'Player{name: $name}';
  }
}
