import '../card/card.dart';
import '../card/card_color.dart';

abstract class Translation {
   late final Map<Card, String> _cardTranslations;
   late final Map<CardColor, String> _cardColorTranslations;
   late final Map<String, String> _stringTranslations;
   final String _internationalLanguageName;
   final String _languageName;

   Translation(this._internationalLanguageName, this._languageName) {
     _cardTranslations = cardTranslation();
     _stringTranslations = stringTranslation();
     _cardColorTranslations = cardColorTranslation();
   }

   Map<Card, String> cardTranslation();
   Map<CardColor, String> cardColorTranslation();
   Map<String, String> stringTranslation();

   String getCardName(Card card) => _cardTranslations[card]!;
   String getCardColorName(CardColor card) => _cardColorTranslations[card]!;
   String? getTranslation(String key) => _stringTranslations[key];
}


