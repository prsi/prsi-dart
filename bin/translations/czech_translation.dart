import 'dart:convert';
import 'dart:io';

import '../card/card.dart';
import '../card/card_color.dart';
import '../card/card_type.dart';
import 'translation.dart';


class CzechTranslation extends Translation {
  static final File _stringTranslationsFile = File('translations/czech.json');
  static const String _internationalLanguageName = 'Czech';
  static const String _languageName = 'Čeština';

  CzechTranslation() : super(_internationalLanguageName, _languageName);

  @override
  Map<CardColor, String> cardColorTranslation() => {
    for (final CardColor cardColor in CardColor.values)
      cardColor : _getNameForColorByCardColor(cardColor)
  };

  @override
  Map<Card, String> cardTranslation() => {
    for (final Card card in Cards.allCards)
      card : '${_getNameForColorByCard(card)} ${_getNameForType(card.type)}'
  };

  @override
  Map<String, String> stringTranslation() {
    Map<String, String> map = {};

    for(MapEntry entry in jsonDecode(_stringTranslationsFile.readAsStringSync()).entries) {
      map[entry.key.toString()] = entry.value.toString();
    }

    return map;
  }

  static String _getNameForType(CardType type) {
    switch (type) {
      case CardType.seven:
        return "VII";
      case CardType.eight:
        return "VIII";
      case CardType.nine:
        return "IX";
      case CardType.x:
        return "X";
      case CardType.eso:
        return "eso";
      case CardType.kral:
        return "král";
      case CardType.menic:
        return "měnič";
      case CardType.svrsek:
        return "svršek";
    }
  }

  static String _getNameForColorByCard(Card card) {
    if(
      card.type == CardType.eso
        || card.type == CardType.menic
        || card.type == CardType.svrsek
        || card.type == CardType.kral
    ) {
      switch (card.color) {
        case CardColor.kule:
          return "kuloví";
        case CardColor.listy:
          return "zelený";
        case CardColor.cerveny:
          return "červený";
        case CardColor.zaludy:
          return "žaludoví";
      }
    } else {
      switch (card.color) {
        case CardColor.kule:
          return "kulová";
        case CardColor.listy:
          return "zelená";
        case CardColor.cerveny:
          return "červená";
        case CardColor.zaludy:
          return "žaludobá";
      }
    }
  }

  String _getNameForColorByCardColor(CardColor color) {
    switch(color) {
      case CardColor.kule:
        return "kule";
      case CardColor.zaludy:
        return "žaludy";
      case CardColor.listy:
        return "listy";
      case CardColor.cerveny:
        return "červený";
    }
  }
}

