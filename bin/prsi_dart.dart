import 'game.dart';
import 'interfaces/cli_interface.dart';
import 'interfaces/game_interface.dart';
import 'player/player.dart';
import 'player/players.dart';
import 'translations/czech_translation.dart';
import 'translations/translation.dart';

void main(List<String> arguments) {
  Translation lang = CzechTranslation();
  GameInterface gameInterface = CLIInterface(lang);
  Set<Player> players = {
    Player("Tomáš1"),
    Player("Tomáš2")
  };

  var game = Game(Players(players), gameInterface);

  game.prepareGame();
  game.startGame();
}
